// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'templates/home-view.html',
            controller : 'HomeCTRler'
        })
        .state('view1', {
            url: '/Bar',
            templateUrl: 'templates/temp-view1.html'
        })
        .state('view4', {
            url: '/view4',
            templateUrl: 'templates/temp-view4.html',
            controller: 'listCTRler'
        })
        .state('view2', {
            url: '/view2',
            templateUrl: 'templates/temp-view2.html'
        })
        .state('view3', {
            url: '/Heart',
            templateUrl: 'templates/temp-view3.html'
        })
        .state('view5', {
            url: '/view5',
            templateUrl: 'templates/temp-view5.html'
        })
        .state('view6', {
            url: '/view6',
            templateUrl: 'templates/card.html',
            controller: 'cartCTRler'
        });

    $urlRouterProvider.otherwise('/home');

})

// create service for controller

.factory('selectedDataStore', function() {
        var storedata = {
            shape: {Heart:{name:'Heart',imgurl:'milk_chocolate_heart_1_1.jpg'}, Bar:{name:'Bar',imgurl:'milk_chocolate_1.png'}},
            base: ['Belgian dark chocolate', 'Belgian milk chocolate', 'Belgian white chocolate']
        };
        var myCartStoreData = new Object();

        return {
            store: storedata,
            cart : myCartStoreData
        };
        //return null;
    })


.controller('HomeCTRler', function($scope, selectedDataStore){
    $scope.store = selectedDataStore.store;

})

.controller('listCTRler', function($scope) {
       
        $scope.items = [{
            name: 'Easter'
        }, {
            name: 'Nuts'
        }, {
            name: 'Fruits'
        }, {
            name: 'Other'
        }, {
            name: 'Candy'
        }];


        $scope.toggleItem = function(item) {
            if ($scope.isItemShow(item)) {
                $scope.showItem = null;
            } else {
                $scope.showItem = item;
            }
        };

        $scope.isItemShow = function(item) {
            return $scope.showItem === item;
        };
    })
    
.controller('cartCTRler', function($scope, selectedDataStore) {
    //get data from facory services selectedDataStore 
    $scope.store = selectedDataStore.store;
    $scope.cart = selectedDataStore.cart;
    console.log($scope.store)
        //$scope.cart = selectedDataStore.cart;

})
