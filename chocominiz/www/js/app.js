// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'templates/home-view.html',
            controller: 'HomeCTRler'
        })
        .state('view1', {
            url: '/Bar',
            templateUrl: 'templates/temp-view1.html',
            controller: 'BCTRler'
        })
        .state('view4', {
            url: '/view4',
            templateUrl: 'templates/temp-view4.html',
            controller: 'listCTRler'
        })
        .state('view2', {
            url: '/view2',
            templateUrl: 'templates/temp-view2.html'
        })
        .state('view3', {
            url: '/Heart',
            templateUrl: 'templates/temp-view3.html',
            controller: 'HCTRler'
        })
        .state('view5', {
            url: '/view5',
            templateUrl: 'templates/temp-view5.html'
        })
        .state('view6', {
            url: '/view6',
            templateUrl: 'templates/cart.html',
            controller: 'cartCTRler'
        });

    $urlRouterProvider.otherwise('/home');

})

// create service for controller

.factory('selectedDataStore', function() {
    /*var storedata = {
        shape: {
            Heart: {
                name: 'Heart',
                imgurl: 'milk_chocolate_heart_1_1.jpg'
            },
            Bar: {
                name: 'Bar',
                imgurl: 'milk_chocolate_1.png'
            }
        },
        baseH: [{
            name: 'dark chocolate',
            imgUrl: 'dark_chocolate_heart.png'
        }, {
            name: 'milk chocolate',
            imgUrl: 'milk_chocolate_heart.png'
        }, {
            name: 'white chocolate',
            imgUrl: 'white_chocolate_heart.png'
        }],
        baseB: [{
            name: 'dark chocolate',
            imgUrl: 'dark_chocolate.png'
        }, {
            name: 'milk chocolate',
            imgUrl: 'milk_chocolate.png'
        }, {
            name: 'white chocolate',
            imgUrl: 'white_chocolate.png'
        }]
    };*/
    var cartData = {};

    var myCartStoreData = function(key, value) {
        cartData[key] = value;
        console.log(cartData.shapename);
    };


    return {
        //store: storedata,
        cart: myCartStoreData,
        Cadata: cartData
    };
    debugger;
    //return null;
})


.controller('HomeCTRler', function($scope, selectedDataStore, $ionicPopover, $http) {
    //$scope.store = selectedDataStore.store;
    $scope.cart = selectedDataStore.cart;
    $scope.Cadata = selectedDataStore.Cadata;
    $http.get('http://localhost:3000/api/shapeModels').success(function(data) {
        $scope.stores = data;
        debugger;
    });
    $scope.selectedShape = function(shape) {
        $scope.cart('shapename', shape.name);
        console.log(shape.name);
    };
    $ionicPopover.fromTemplateUrl('templates/card.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
})

.controller('listCTRler', function($scope, selectedDataStore, $ionicPopover) {
    $scope.Cadata = selectedDataStore.Cadata;
    $scope.items = [{
        name: 'Easter'
    }, {
        name: 'Nuts'
    }, {
        name: 'Fruits'
    }, {
        name: 'Other'
    }, {
        name: 'Candy'
    }];


    $scope.toggleItem = function(item) {
        if ($scope.isItemShow(item)) {
            $scope.showItem = null;
        } else {
            $scope.showItem = item;
        }
    };

    $scope.isItemShow = function(item) {
        return $scope.showItem === item;
    };

    $ionicPopover.fromTemplateUrl('templates/card.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
})

.controller('cartCTRler', function($scope, selectedDataStore) {
    //get data from facory services selectedDataStore 
    //$scope.store = selectedDataStore.store;
    $scope.cart = selectedDataStore.cart;
    $scope.Cadata = selectedDataStore.Cadata;
    //console.log($scope.store)
    console.log($scope.Cadata);
    //$scope.cart = selectedDataStore.cart;

})

.controller('HCTRler', function($scope, selectedDataStore, $http, $ionicPopover) {
    //$scope.store = selectedDataStore.store;
    $scope.cart = selectedDataStore.cart;
    $scope.Cadata = selectedDataStore.Cadata;
    
    $http.get('http://localhost:3000/api/baseModels').success(function(data) {
        $scope.store = data[0];
        debugger;
    });
    $scope.selectedBase = function(base) {
        $scope.cart('basename', base.name);
        console.log(base.name);
    };
    $ionicPopover.fromTemplateUrl('templates/card.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
})

.controller('BCTRler', function($scope, selectedDataStore, $ionicPopover, $http) {
    //$scope.store = selectedDataStore.store;
    $scope.cart = selectedDataStore.cart;
    $scope.Cadata = selectedDataStore.Cadata;

    $http.get('http://localhost:3000/api/baseModels').success(function(data) {
        $scope.store = data[0];
    });

    $scope.selectedBase = function(base) {
        $scope.cart('basename', base.name);
        console.log(base.name);
    };
    $ionicPopover.fromTemplateUrl('templates/card.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
})
